using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;

public class Collectible : MonoBehaviour, ICoordinate
{
	public int score;
	public AnimationClip animationClip;
	public ParticleSystem collectEffectPrefab;

	private new Renderer renderer;
	private Canvas canvas;

	public int X { get { return (int)transform.position.x; } }
	public int Y { get { return (int)transform.position.y; } }

	public virtual bool ShouldCollectCreateNew
	{
		get { return true; }
	}

	public void Setup()
	{
		renderer = gameObject.GetComponent<Renderer>();
		if (renderer == null)
		{
			Debug.LogError("A renderer is missing from the collectible!");
		}

		canvas = gameObject.GetComponentInChildren<Canvas>(includeInactive: true);
		if (canvas == null)
		{
			Debug.LogError("Couldn't find canvas from collectible's children!");
		}

		TMP_Text scoreText = GetComponentInChildren<TMP_Text>(includeInactive: true);
		if (scoreText != null)
		{
			scoreText.text = score.ToString();
		}
	}

	public virtual void OnCollect(SnakeController snake)
	{
		// 1. Grant the score to the player
		GameManager.Instance.AddScore(score);

		// 2. Grow the snake
		snake.Grow();

		// 3. Notify the board that the apple has been collected
		if (ShouldCollectCreateNew)
		{
			LevelController.Current.GameBoard.NotifyItemCollected(this);
		}

		// 4. Hides the apple
		renderer.enabled = false;

		float waitTime = animationClip.length;

		// 5. Enable in-world UI for the collectible
		canvas.gameObject.SetActive(true);

		// 6. Play collect sound
		AudioSource audio = GetComponent<AudioSource>();
		if (audio != null)
		{
			GameManager.Instance.AudioManager.PlaySoundEffect(
				audio, SoundEffectType.Collect);
		}

		// 7. Play particle effect
		if (collectEffectPrefab != null)
		{
			ParticleSystem collectEffect =
				Instantiate(collectEffectPrefab, transform);

			if (collectEffect.main.duration > waitTime)
			{
				waitTime = collectEffect.main.duration;
			}

			collectEffect.Play(withChildren: true);
		}

		StartCoroutine(DestroyCollectible(waitTime));
	}

	private IEnumerator DestroyCollectible(float waitTime)
	{
		yield return new WaitForSeconds(waitTime); // Waits until the animation is completed

		Destroy(gameObject);
	}
}
