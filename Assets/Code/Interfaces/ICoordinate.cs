public interface ICoordinate
{
	int X { get; }
	int Y { get; }
}
