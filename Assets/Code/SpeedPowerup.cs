using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPowerup : Collectible
{
	public float speedIncrease = 1f;

	public override bool ShouldCollectCreateNew
	{
		get { return false; }
	}

	public override void OnCollect(SnakeController snake)
	{
		// 1. Speed up the snake
		snake.SpeedUp(speedIncrease);

		// 2. Call Collectible's functionality
		base.OnCollect(snake);
	}
}
