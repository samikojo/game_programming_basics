using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeController : MonoBehaviour, ICoordinate
{
	public enum Direction
	{
		None = 0,
		Up,
		Down,
		Left,
		Right
	};

	[Tooltip("The initial speed of the snake"), Range(0.1f, 10f)]
	public float speed = 1;

	[Tooltip("The initial length of the snake"), Range(2, 6)]
	public int length = 1;

	public GameObject headPrefab;
	public GameObject bodyPrefab;
	public GameObject tailPrefab;

	private GameObject head;
	private List<GameObject> body = new List<GameObject>();
	private GameObject tail;
	private Timer moveTimer;
	private bool isInitialized = false;

	// The direction user want's to move the snake next
	private Direction inputDirection = Direction.None;
	// The direction the snake is moving currently
	private Direction movementDirection = Direction.Up;

	// These 2 variables are used when the snake grows
	private Vector3 previousTailPosition;
	private Quaternion previousTailRotation;

	public int Length
	{
		get
		{
			// Length of the snake's body + head + tail
			return body.Count + 2;
		}
	}

	public int X { get { return (int)head.transform.position.x; } }
	public int Y { get { return (int)head.transform.position.y; } }

	#region Unity messages
	void Awake()
	{
		moveTimer = gameObject.AddComponent<Timer>();
	}

	// Update is called once per frame
	void Update()
	{
		// If the object isn't initialized yet, we can't execute update
		if (!isInitialized) { return; }

		// Read input from the player
		Vector2 input = ReadInput();
		InterpretInput(input);

		// Snake's current location in the game world
		// Old smooth movement example. You can use this as a reference
		// Vector3 movement = new Vector3(input.x, input.y, 0) * speed * Time.deltaTime;
		// transform.position += movement;

		if (moveTimer.IsCompleted)
		{
			this.movementDirection = GetMovementDirection(this.inputDirection);
			Move(this.movementDirection);
			this.inputDirection = Direction.None;
			SetTimer();

			// Collision checks
			// Did we hit a collectible?
			LevelController.Current.GameBoard.CheckCollision(this);

			// Did we hit an obstacle?
			if (LevelController.Current.GameBoard.IsOutOfBounds(this)
				|| DoesCollideWithItself())
			{
				int highscore = PlayerPrefs.GetInt(Config.HighScoreKey, 0);
				if (GameManager.Instance.Score > highscore)
				{
					// New high score! Let's update the value
					PlayerPrefs.SetInt(Config.HighScoreKey, GameManager.Instance.Score);
					GameManager.Instance.IsNewHighScore = true;
				}

				GameManager.Instance.Go(GameStateBase.Type.GameOver);
			}
		}
	}

	private bool DoesCollideWithItself()
	{
		int tailX = (int)tail.transform.position.x;
		int tailY = (int)tail.transform.position.y;

		if (X == tailX && Y == tailY)
		{
			// Collision with the tail
			return true;
		}

		foreach (GameObject bodyPiece in body)
		{
			int bodyX = (int)bodyPiece.transform.position.x;
			int bodyY = (int)bodyPiece.transform.position.y;

			if (X == bodyX && Y == bodyY)
			{
				return true;
			}
		}

		return false;
	}

	public void Grow()
	{
		GameObject bodyPiece = InstantiatePrefab(bodyPrefab, tail.transform.position, tail.transform.rotation,
			transform);

		tail.transform.position = previousTailPosition;
		tail.transform.rotation = previousTailRotation;

		LevelController.Current.GameBoard.ReserveCell((int)tail.transform.position.x, (int)tail.transform.position.y);

		body.Add(bodyPiece);
	}

	public void SpeedUp(float speedIncrease)
	{
		// Increase the snake's speed by speedIncrease
		speed += speedIncrease;
	}

	private void Move(Direction movementDirection)
	{
		// Move the snake by moving each snake part separately
		// 1: Start from tail -> move it to the position where the next bodypart is at the moment
		// List indexing start from zero so the last bodypart will be located at the index body.Count - 1
		int parentIndex = body.Count - 1;
		GameObject currentBodyPart = body[parentIndex];

		// Release tail's previous cell
		LevelController.Current.GameBoard.ReleaseCell((int)tail.transform.position.x, (int)tail.transform.position.y);

		// Cache tail's previous position and rotation just before moving it
		previousTailPosition = tail.transform.position;
		previousTailRotation = tail.transform.rotation;

		// Move the tail where the last snake body piece is located at the moment.
		// Remember to update the rotation as well		
		tail.transform.position = currentBodyPart.transform.position;
		tail.transform.rotation = currentBodyPart.transform.rotation;

		// 2: Move the next bodypart where the part in front of this is now
		// Go through all bodyparts using a reversed for-loop
		// 3: Repeat until we reach the head
		for (int i = parentIndex; i >= 0; i--)
		{
			parentIndex--;

			if (parentIndex >= 0)
			{
				// Move a bodypart where the next bodypart is atm.
				body[i].transform.position = body[parentIndex].transform.position;
				body[i].transform.rotation = body[parentIndex].transform.rotation;
			}
			else
			{
				// Move the first bodypart where the head is
				body[i].transform.position = head.transform.position;
				body[i].transform.rotation = head.transform.rotation;
			}
		}

		MoveHead(movementDirection);
	}

	private void MoveHead(Direction movementDirection)
	{
		int dx = 0; // How much we move the head along the x axis.
		int dy = 0; // How much we move the head along the y axis.
		int rotationZ = 0; // To what direction we rotate the head to.'

		switch (movementDirection)
		{
			case Direction.Up:
				dy = 1;
				rotationZ = 0;
				break;
			case Direction.Down:
				dy = -1;
				rotationZ = 180;
				break;
			case Direction.Left:
				dx = -1;
				rotationZ = 90;
				break;
			case Direction.Right:
				dx = 1;
				rotationZ = -90;
				break;
		}

		Vector3 movement = new Vector3(dx, dy, 0);
		head.transform.position += movement;

		LevelController.Current.GameBoard.ReserveCell((int)head.transform.position.x, (int)head.transform.position.y);

		head.transform.rotation = Quaternion.Euler(0, 0, rotationZ);
	}

	private Direction GetMovementDirection(Direction inputDirection)
	{
		if (inputDirection == this.movementDirection
			|| inputDirection == Direction.None
			|| GetOpposite(inputDirection) == this.movementDirection)
		{
			return this.movementDirection;
		}

		return inputDirection;
	}

	private Direction GetOpposite(Direction direction)
	{
		switch (direction)
		{
			case Direction.Up: return Direction.Down;
			case Direction.Down: return Direction.Up;
			case Direction.Left: return Direction.Right;
			case Direction.Right: return Direction.Left;
			default: return Direction.None;
		}
	}

	private void InterpretInput(Vector2 input)
	{
		if (this.inputDirection == Direction.None)
		{
			// The input isn't given yet. Let's choose the next movement direction based on the user input.
			if (input.y > 0)
			{
				this.inputDirection = Direction.Up;
			}
			else if (input.y < 0)
			{
				this.inputDirection = Direction.Down;
			}
			else if (input.x > 0)
			{
				this.inputDirection = Direction.Right;
			}
			else if (input.x < 0)
			{
				this.inputDirection = Direction.Left;
			}
		}
	}

	#endregion

	private void SetTimer()
	{
		// speed = distance / time => time = distance  / speed
		float time = 1 / speed;
		moveTimer.Set(time);
		moveTimer.Run();
	}

	public void Setup()
	{
		Vector3 position = Vector3.zero;
		this.head = InstantiatePrefab(this.headPrefab, position, Quaternion.identity, transform);

		while (Length < this.length)
		{
			position.y -= 1;
			GameObject bodyObject = InstantiatePrefab(this.bodyPrefab, position, Quaternion.identity, transform);
			this.body.Add(bodyObject);
		}

		position.y -= 1;
		this.tail = InstantiatePrefab(this.tailPrefab, position, Quaternion.identity, transform);

		SetTimer();

		isInitialized = true;
	}

	private GameObject InstantiatePrefab(GameObject prefab, Vector3 position, Quaternion rotation,
		Transform parent)
	{
		GameObject obj = Instantiate(prefab, position, rotation, parent);
		if (!LevelController.Current.GameBoard.ReserveCell((int)obj.transform.position.x, (int)obj.transform.position.y))
		{
			Debug.LogError($"Can't reserve cell for object {obj}");
		}

		return obj;
	}

	private static Vector2 ReadInput()
	{
		float horizontal = Input.GetAxisRaw("Horizontal");
		float vertical = Input.GetAxisRaw("Vertical");

		return new Vector2(horizontal, vertical);
	}
}
