using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour
{
	[SerializeField]
	private Slider slider;

	[SerializeField]
	private AudioManager.SoundGroup targetGroup;

	public float Value
	{
		get { return slider.value; }
	}

	private void Start()
	{
		SetVolume();
	}

	private void SetVolume()
	{
		float normalized = GameManager.Instance.AudioManager.GetVolume(targetGroup);
		if (normalized < 0)
		{
			Debug.LogError("Error! Can't read the volume for the Mixer Group " +
				targetGroup);
			return;
		}

		slider.value = normalized;
	}
}
