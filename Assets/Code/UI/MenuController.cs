using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MenuController : MonoBehaviour
{
	public TMP_Text highScoreText;

	private const string HighScoreText = "High score: {0}";

	private void Start()
	{
		int highscore = PlayerPrefs.GetInt(Config.HighScoreKey, defaultValue: 0);
		if (highScoreText != null)
		{
			highScoreText.text = string.Format(HighScoreText, highscore);
		}
	}

	public void StartGame()
	{
		GameManager.Instance.Go(GameStateBase.Type.Level);
	}

	public void ToOptions()
	{
		GameManager.Instance.Go(GameStateBase.Type.Options);
	}

	public void Quit()
	{
		Application.Quit();
	}
}
