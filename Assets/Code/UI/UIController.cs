using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIController : MonoBehaviour
{
	public TMP_Text scoreText;

	public void SetScore(int score)
	{
		scoreText.text = $"Score: {score}";
	}
}
