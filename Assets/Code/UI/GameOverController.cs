using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameOverController : MonoBehaviour
{
	public TMP_Text highScoreText;

	private void Start()
	{
		if (GameManager.Instance.IsNewHighScore)
		{
			highScoreText.text = $"New high score {GameManager.Instance.Score}!";
		}
		else
		{
			highScoreText.text = $"Your score was {GameManager.Instance.Score}.";
		}
	}

	public void Restart()
	{
		GameManager.Instance.Go(GameStateBase.Type.Level);
	}

	public void BackToMenu()
	{
		GameManager.Instance.Go(GameStateBase.Type.Menu);
	}

	public void Quit()
	{
		Application.Quit();
	}
}
