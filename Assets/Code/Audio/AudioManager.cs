using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
	private const float MinVol = -80;
	private const float MaxVol = 20;

	public enum SoundGroup
	{
		None = 0,
		Master,
		Music,
		SFX
	}

	[SerializeField]
	private AudioContainer audioData;

	[SerializeField]
	private AudioMixer mixer;

	private void Start()
	{
		float masterVolume = LoadVolume(SoundGroup.Master);
		float musicVolume = LoadVolume(SoundGroup.Music);
		float sfxVolume = LoadVolume(SoundGroup.SFX);

		// SetVolume(SoundGroup.Master, masterVolume);
		// SetVolume(SoundGroup.Music, musicVolume);
		// SetVolume(SoundGroup.SFX, sfxVolume);
	}

	private float LoadVolume(SoundGroup group)
	{
		string volumeName = audioData.GetVolumeParamName(group);
		if (volumeName != null)
		{
			return PlayerPrefs.GetFloat(volumeName, audioData.GetDefaultVolume(group));
		}

		return -1;
	}

	/// <summary>
	/// Plays an audio effect for requested type.
	/// </summary>
	/// <param name="source">Audio source</param>
	/// <param name="effectType">The type of the audio effect</param>
	/// <returns>The length of the audio clip played. -1, if no suitable audio clip
	/// is found.</returns>
	public float PlaySoundEffect(AudioSource source, SoundEffectType effectType)
	{
		AudioClip clip = audioData.GetAudioClipByType(effectType);
		if (clip != null)
		{
			source.PlayOneShot(clip);
			return clip.length;
		}

		return -1.0f;
	}

	/// <summary>
	/// Returns the volume normalized to the range of [0, 1]
	/// </summary>
	/// <param name="group">The sound group volume is requested for</param>
	/// <returns>The saved normalized volume. If sound group is not valid,
	/// -1 is returned.</returns>
	public float GetVolume(SoundGroup group)
	{
		string volumeParamName = audioData.GetVolumeParamName(group);
		if (volumeParamName != null &&
			mixer.GetFloat(volumeParamName, out float volume))
		{
			float normalized = ToLinear(volume);
			return normalized;
		}

		return -1;
	}

	public bool SetVolume(SoundGroup group, float normalized)
	{
		string volumeParamName = audioData.GetVolumeParamName(group);
		if (volumeParamName != null &&
			mixer.SetFloat(volumeParamName, normalized))
		{
			PlayerPrefs.SetFloat(volumeParamName, normalized);
			return true;
		}

		return false;
	}

	public float ToDB(float linear)
	{
		if (linear <= 0)
		{
			return -80.0f;
		}

		return Mathf.Log10(linear) * 20.0f;
	}

	public float ToLinear(float volume)
	{
		return Mathf.Clamp01(Mathf.Pow(10.0f, volume / 20.0f));
	}
}
