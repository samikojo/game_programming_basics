using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
	public Collectible applePrefab;

	public Collectible proteinPrefab;

	public float proteinSpawnInterval = 1;
	public float proteinSpawnOffset = 1;

	private List<Cell> grid = new List<Cell>();
	private List<Cell> freeCells = new List<Cell>();

	private Renderer backgroundRenderer;
	private int minX = 0;
	private int maxX = 0;
	private int minY = 0;
	private int maxY = 0;

	private Collectible apple;
	private Collectible protein;

	private Timer proteinTimer;

	#region Unity Messages
	private void Reset()
	{
		InitializeGrid();
	}

	private void OnDrawGizmos()
	{
		foreach (Cell cell in grid)
		{
			if (cell.IsFree)
			{
				Gizmos.color = Color.green;
			}
			else
			{
				Gizmos.color = Color.red;
			}

			Gizmos.DrawWireCube(new Vector3(cell.X, cell.Y), new Vector3(0.95f, 0.95f, 0.95f));
		}
	}

	private void Update()
	{
		if (proteinTimer.IsCompleted)
		{
			// Instantiate the protein powerup
			CreateProtein();
		}
	}

	#endregion

	public void Setup()
	{
		InitializeGrid();
		InitializeProteinTimer();
	}

	private void InitializeProteinTimer()
	{
		if (proteinTimer == null)
		{
			proteinTimer = GetComponent<Timer>();
			if (proteinTimer == null)
			{
				proteinTimer = gameObject.AddComponent<Timer>();
			}
		}

		float randomOffset = Random.Range(-proteinSpawnOffset, proteinSpawnOffset);
		float instantiateTime = proteinSpawnInterval + randomOffset;
		proteinTimer.Set(instantiateTime);
		proteinTimer.Run();
	}

	public void CreateApple()
	{
		apple = InstantiateCollectible(applePrefab);
	}

	public void CreateProtein()
	{
		if (protein == null)
		{
			protein = InstantiateCollectible(proteinPrefab);
		}

		proteinTimer.Reset();
		proteinTimer.Run();
	}

	private Collectible InstantiateCollectible(Collectible prefab)
	{
		Cell cell = GetRandomFreeCell();
		Vector3 position = new Vector3(cell.X, cell.Y, 0);
		Collectible collectible = Instantiate(prefab, position, Quaternion.identity, transform);
		collectible.Setup();
		ReserveCell(cell);
		return collectible;
	}

	public void CheckCollision(SnakeController snake)
	{
		// Did the snake hit the apple?
		CheckCollisionWithCollectible(snake, apple);

		// Did the snake hit the protein bar?
		if (protein != null)
		{
			CheckCollisionWithCollectible(snake, protein);
		}
	}

	private void CheckCollisionWithCollectible(SnakeController snake, Collectible collectible)
	{
		if (snake.X == collectible.X && snake.Y == collectible.Y)
		{
			// The snake collided with the apple. Let's collect it.
			collectible.OnCollect(snake);
		}
	}

	public bool IsOutOfBounds(ICoordinate coordinate)
	{
		return coordinate.X < minX
			|| coordinate.Y < minY
			|| coordinate.X >= maxX
			|| coordinate.Y >= maxY;
	}

	public void NotifyItemCollected(Collectible collectible)
	{
		// TODO: Release the cell where the previous apple was
		// The snake does that currently
		if (collectible.ShouldCollectCreateNew)
		{
			CreateApple();
		}

		if (collectible == protein)
		{
			protein = null;
		}
	}

	#region Grid functionality
	private void InitializeGrid()
	{
		backgroundRenderer = GetComponent<Renderer>();
		if (backgroundRenderer == null)
		{
			Debug.LogError("ERROR: Background renderer cannot be found!");
		}

		Debug.Log("Initialize grid");

		// For editor only. Removes old cells before creating new ones
		grid.Clear();

		maxX = Mathf.CeilToInt(backgroundRenderer.bounds.max.x);
		minX = Mathf.CeilToInt(backgroundRenderer.bounds.min.x + 0.5f);
		maxY = Mathf.CeilToInt(backgroundRenderer.bounds.max.y);
		minY = Mathf.CeilToInt(backgroundRenderer.bounds.min.y + 0.5f);

		for (int y = minY; y < maxY; y++)
		{
			for (int x = minX; x < maxX; x++)
			{
				Cell currentCell = new Cell(x, y, true);
				grid.Add(currentCell);
				freeCells.Add(currentCell);
			}
		}
	}

	public Cell GetRandomFreeCell()
	{
		int randonIndex = Random.Range(0, freeCells.Count);
		return freeCells[randonIndex];
	}

	/// <summary>
	/// Reserves a cell from the grid.
	/// </summary>
	/// <param name="x">The x-coordinate of the cell</param>
	/// <param name="y">The y-coordinate of the cell</param>
	/// <returns>True, if cell was free and the coordinate was legal. False otherwise</returns>
	public bool ReserveCell(int x, int y)
	{
		Cell cell = GetCell(x, y);
		return ReserveCell(cell);
	}

	/// <summary>
	/// Reserves a cell from the grid.
	/// </summary>
	/// <param name="cell">The cell which should be reserved.</param>
	/// <returns>True, if cell was free and the coordinate was legal. False otherwise</returns>
	public bool ReserveCell(Cell cell)
	{
		if (cell == null || !cell.IsFree)
		{
			return false;
		}

		// TODO: Reserve the node for a particular object, don't just mark it as reserved.
		cell.IsFree = false;
		freeCells.Remove(cell);
		return true;
	}

	public bool ReleaseCell(Cell cell)
	{
		if (cell == null || cell.IsFree)
		{
			return false;
		}

		// TODO: Reserve the node for a particular object, don't just mark it as reserved.
		cell.IsFree = true;
		freeCells.Add(cell);
		return true;
	}

	public bool ReleaseCell(int x, int y)
	{
		Cell cell = GetCell(x, y);
		return ReleaseCell(cell);
	}

	private Cell GetCell(int x, int y)
	{
		if (y < minY || y > maxY || x < minX || x > maxX)
		{
			// The coordinate is outside of the game board.
			// Let's use the return value of null to indicate that.
			return null;
		}

		foreach (Cell cell in grid)
		{
			if (cell.X == x && cell.Y == y)
			{
				return cell;
			}
		}

		Debug.LogError($"Couldn't find a cell for a coordinate {x}, {y}. " +
			"This is a bug! Please report this to the developer");
		return null;
	}
	#endregion
}
